def test_home_page(test_app):
    client = test_app.test_client()
    resp = client.get("/")
    assert resp.status_code == 200
    assert b"Daily Poshmark Reminders" in resp.data
    assert b"Login" in resp.data
    assert b"Logout" not in resp.data
