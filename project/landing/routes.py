from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_user, logout_user
from werkzeug.urls import url_parse

from project import db
from project.forms import LoginForm, RegistrationForm
from project.models import User

landing_blueprint = Blueprint("landing", __name__)


@landing_blueprint.route("/", methods=["GET", "POST"])
def index():
    return render_template("landing/index.html", title="Home")


@landing_blueprint.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("dashboard.index"))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        db.session.close()
        flash("Congratulations, you are now a registered user!", "success")

        login_user(user)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("dashboard.index")
        return redirect(next_page)
    return render_template("landing/register.html", title="Register", form=form)


@landing_blueprint.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("landing.index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password", "danger")
            return redirect(url_for("landing.login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("dashboard.index")
        return redirect(next_page)
    return render_template("landing/login.html", title="Sign In", form=form)


@landing_blueprint.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("landing.index"))
