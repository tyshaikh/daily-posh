from flask import Blueprint, flash, jsonify, redirect, render_template, url_for
from flask_login import current_user, login_required

from project import db
from project.forms import ProductForm
from project.models import Product

dashboard_blueprint = Blueprint("dashboard", __name__)


@dashboard_blueprint.route("/dashboard", methods=["GET", "POST"])
@login_required
def index():
    product_count = db.session.query(Product).filter_by(owner=current_user).count()
    db.session.close()
    form = ProductForm()
    if form.validate_on_submit():
        if product_count >= 5:
            flash(
                """Free accounts only get 5 saved searches,
                  upgrade your account!""",
                "warning",
            )
            return redirect(url_for("dashboard.index"))

        product = Product(title=form.title.data, url=form.url.data, owner=current_user)
        db.session.add(product)
        db.session.commit()
        db.session.close()
        flash("Your search is now saved!", "success")
        return redirect(url_for("dashboard.index"))

    products = db.session.query(Product).filter_by(owner=current_user)
    return render_template(
        "dashboard/index.html",
        title="Dashboard",
        form=form,
        products=products,
        count=product_count,
    )


@dashboard_blueprint.route("/delete/<product_id>", methods=["POST"])
def delete(product_id):
    product = Product.query.filter_by(id=product_id, owner=current_user).first()
    if product is None:
        flash("That search is not in the database!", "warning")
        return redirect(url_for("dashboard.index"))
    db.session.delete(product)
    db.session.commit()
    db.session.close()
    flash("Your search has been removed!", "success")
    return redirect(url_for("dashboard.index"))


@dashboard_blueprint.route("/async_delete/<product_id>", methods=["POST"])
def async_delete(product_id):
    product = Product.query.filter_by(id=product_id, owner=current_user).first()
    db.session.delete(product)
    db.session.commit()
    db.session.close()
    return jsonify(status="success")
