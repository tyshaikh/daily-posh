# from flask import render_template, flash, redirect, request, url_for, jsonify
# from project import app, db
# from flask_login import current_user, login_user, logout_user, login_required
# from project.models import User, Product
# from project.forms import LoginForm, RegistrationForm, ProductForm
# from werkzeug.urls import url_parse
#
# # Public Routes
# @app.route("/", methods=["GET", "POST"])
# def index():
#     return render_template("landing.html", title="Home")
#
#
# # Auth Routes
# @app.route("/register", methods=["GET", "POST"])
# def register():
#     if current_user.is_authenticated:
#         return redirect(url_for("dashboard"))
#     form = RegistrationForm()
#     if form.validate_on_submit():
#         user = User(username=form.username.data, email=form.email.data)
#         user.set_password(form.password.data)
#         db.session.add(user)
#         db.session.commit()
#         db.session.close()
#         flash("Congratulations, you are now a registered user!", "success")
#
#         login_user(user)
#         next_page = request.args.get("next")
#         if not next_page or url_parse(next_page).netloc != "":
#             next_page = url_for("dashboard")
#         return redirect(next_page)
#     return render_template("register.html", title="Register", form=form)
#
#
# @app.route("/login", methods=["GET", "POST"])
# def login():
#     if current_user.is_authenticated:
#         return redirect(url_for("index"))
#     form = LoginForm()
#     if form.validate_on_submit():
#         user = User.query.filter_by(username=form.username.data).first()
#         if user is None or not user.check_password(form.password.data):
#             flash("Invalid username or password", "danger")
#             return redirect(url_for("login"))
#         login_user(user, remember=form.remember_me.data)
#         next_page = request.args.get("next")
#         if not next_page or url_parse(next_page).netloc != "":
#             next_page = url_for("dashboard")
#         return redirect(next_page)
#     return render_template("login.html", title="Sign In", form=form)
#
#
# @app.route("/logout")
# def logout():
#     logout_user()
#     return redirect(url_for("index"))
#
#
# # Search Dashboard Routes
# @app.route("/dashboard", methods=["GET", "POST"])
# @login_required
# def dashboard():
#     product_count = db.session.query(Product).filter_by(owner=current_user).count()
#     db.session.close()
#     form = ProductForm()
#     if form.validate_on_submit():
#         if product_count >= 5:
#             flash(
#                 """Free accounts only get 5 saved searches,
#                   upgrade your account!""",
#                 "warning",
#             )
#             return redirect(url_for("dashboard"))
#
#         product = Product(title=form.title.data, url=form.url.data, owner=current_user)
#         db.session.add(product)
#         db.session.commit()
#         db.session.close()
#         flash("Your search is now saved!", "success")
#         return redirect(url_for("dashboard"))
#
#     products = db.session.query(Product).filter_by(owner=current_user)
#     return render_template(
#         "dashboard.html",
#         title="Dashboard",
#         form=form,
#         products=products,
#         count=product_count,
#     )
#
#
# @app.route("/delete/<product_id>", methods=["POST"])
# def delete(product_id):
#     product = Product.query.filter_by(id=product_id, owner=current_user).first()
#     if product is None:
#         flash("That search is not in the database!", "warning")
#         return redirect(url_for("dashboard"))
#     db.session.delete(product)
#     db.session.commit()
#     db.session.close()
#     flash("Your search has been removed!", "success")
#     return redirect(url_for("dashboard"))
#
#
# @app.route("/async_delete/<product_id>", methods=["POST"])
# def async_delete(product_id):
#     product = Product.query.filter_by(id=product_id, owner=current_user).first()
#     db.session.delete(product)
#     db.session.commit()
#     db.session.close()
#     return jsonify(status="success")
