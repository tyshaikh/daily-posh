import os
import glob
import pickle
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import date

from html2text import html2text

from s3_helper import download_file


SENDER_EMAIL = os.getenv('EMAIL_ACCOUNT')
SENDER_PASSWORD = os.getenv('EMAIL_PASSWORD')
TODAY = date.today().strftime("%m-%d-%Y")


def process_file(file):
    "Process each file by generating markup and sending an email"
    scraping_store = pickle.load(open(file, "rb"))
    output = [email_record(item) for item in scraping_store]

    return output


def email_record(item):
    "Run markup generation and email sending for each user"
    email = item['email']
    results = item['listings']
    markup = combine_markup(results)

    print("Sending email to:", email)
    send_email(markup, email, SENDER_EMAIL, SENDER_PASSWORD)

    return "Success"


def combine_markup(results):
    "Combine the generated markup for each saved search"
    markup_sections = [generate_markup(result['title'], result['items'])
                       for result in results]
    combined_markup = ' '.join(markup_sections)

    return combined_markup


def generate_markup(title, collection):
    "Generate email markup from recent items"
    html = "<div style='text-align: center;'>"
    heading = f"""<p style="font-size: 1.5rem; font-weight: bold;">
              {title}&nbsp;<span style="font-size: 1rem;">
              ({len(collection)} items)</span></p>"""

    card_elements = [extract_values(item) for item in collection]
    item_group = ' '.join(card_elements)
    combined_html = html + heading + item_group

    return combined_html


def extract_values(item):
    "Take values from product card and place them in styled HTML"
    title = item[0]
    price = item[1]
    link = item[2]
    img = item[3]

    card_element = f"""<a style="text-decoration: none;" href="{link}"
                   target="_blank">
                   <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                   width: 300px; margin: 0 auto;">
                   <img src="{img}" style="max-width: 100%">
                   <div style="padding: 2px 16px;">
                   <p style="font-size: 1.3rem; font-weight: bold;">
                   {title} ({price})</p></div></div></a></div><br>"""

    return card_element


def send_email(items, recipient, sender, sender_password):
    "Send email through dummy Gmail account"
    sender_email = sender
    password = sender_password
    receiver_email = recipient

    message = MIMEMultipart("alternative")
    message["Subject"] = "Latest Poshmark Listings"
    message["From"] = sender_email
    message["To"] = receiver_email

    html = """    <html>
      <body>
      <div style="margin: 0 auto;">
        <!-- Heading -->
        <div>
          <h1 style="text-align: center;">New Poshmark Listings</h1>
          <hr>
        </div>
        <br>
    """ + items + """
      </div>
      </body>
    </html>
    """
    text = html2text(html)
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(str(html), "html")
    message.attach(part1)
    message.attach(part2)

    s = smtplib.SMTP('smtp.mailgun.org', 587)

    s.login(sender_email, password)
    s.sendmail(sender_email, receiver_email, message.as_string())
    s.quit()


if __name__ == '__main__':
    filename = "scraping-{date}.p".format(date=TODAY)
    download_file(filename)
    process_file(filename)
