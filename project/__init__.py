import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager


db = SQLAlchemy(session_options={"expire_on_commit": False})
migrate = Migrate()
login = LoginManager()
login.login_view = "login"


def create_app(script_info=None):

    app = Flask(__name__)

    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)

    from project.landing.routes import landing_blueprint

    app.register_blueprint(landing_blueprint)

    from project.dashboard.routes import dashboard_blueprint

    app.register_blueprint(dashboard_blueprint)

    from project.errors.routes import errors_blueprint

    app.register_blueprint(errors_blueprint)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
