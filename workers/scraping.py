import glob
import pickle
from datetime import date
from time import sleep
from requests import get
from bs4 import BeautifulSoup
from dateutil.parser import parse
from datetime import datetime, timedelta

from s3_helper import upload_file, download_file


HEADER = {'user-agent': """Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"""}
DAYS = 2
TODAY = date.today().strftime("%m-%d-%Y")


def run_search(search_url):
    "Pull down search results and extract out product cards"
    response = get(search_url, headers=HEADER)
    html_soup = BeautifulSoup(response.text, 'html.parser')
    item_container = html_soup.find_all('div', class_='tile')

    return item_container


def get_attributes(soup_obj):
    "Extract product values from card"
    price = soup_obj['data-post-price']

    url_tag = soup_obj.a
    url = "https://poshmark.com" + url_tag['href']
    title = url_tag['title']

    img_tag = url_tag.img
    img = img_tag['src']

    return (title, price, url, img)


def get_days(soup_obj):
    "Convert to EST and return difference in days"
    created_date = soup_obj['data-created-at']

    pst_date = parse(created_date, ignoretz=True)
    est_date = pst_date + timedelta(hours=3)

    now = datetime.now()
    diff = abs((est_date-now).days)

    return diff


def get_cards(soup_obj):
    "Return the card values if it is less than days constant"
    difference = get_days(soup_obj)

    if difference >= DAYS:
        return None
    else:
        card_values = get_attributes(soup_obj)
        return card_values


def get_recent_items(search_obj):
    "Get all recent items"
    title = search_obj[0]
    link = search_obj[1]

    product_cards = run_search(link)
    card_data = [get_cards(card) for card in product_cards]
    card_data = list(filter(None, card_data))

    summary = f'Found {len(card_data)} items posted in the last {DAYS} days'
    print(summary)

    return (title, card_data) if len(card_data) > 0 else None


def scrape_record(item):
    "Run scraping process on each user record"
    email = item[0][2]
    searches = item[1]

    user_results = {'email': email, 'listings': []}
    recent_items = [get_recent_items(search) for search in searches]
    recent_items = list(filter(None, recent_items))

    listings = [{"title": title, "items": items}
                for title, items in recent_items]
    user_results['listings'].extend(listings)

    sleep(5)

    return user_results


def process_file(file):
    "Process each file and export the results of the mailer script"
    db_store = pickle.load(open(file, "rb"))

    scraping_store = [scrape_record(item) for item in db_store]
    filename = "scraping-{date}.p".format(date=TODAY)
    pickle.dump(scraping_store, open(filename, "wb"))
    upload_file(filename)
    print('Uploaded: ', filename, 'to S3')

    return "Success"


if __name__ == '__main__':
    filename = "etl-{date}.p".format(date=TODAY)
    download_file(filename)
    process_file(filename)
