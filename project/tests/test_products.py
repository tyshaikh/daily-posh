import json

from project.tests.utils import add_user, recreate_db


def test_add_product(test_app, test_database):
    recreate_db()
    user = add_user("elle", "elle@gmail.com", "password")
    client = test_app.test_client()
    resp = client.post(
        "/login",
        data=json.dumps({"username": "elle", "password": "password"}),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Hi, elle!" in resp.data

    resp = client.post(
        "/dashboard",
        data=json.dumps(
            {
                "title": "Diesel Jeans",
                "url": "https://poshmark.com/brand/Diesel",
                "owner": user.id,
            }
        ),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Diesel" in resp.data


def test_add_product_missing_link(test_app, test_database):
    recreate_db()
    user = add_user("elle", "elle@gmail.com", "password")
    client = test_app.test_client()
    resp = client.post(
        "/login",
        data=json.dumps({"username": "elle", "password": "password"}),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Hi, elle!" in resp.data

    resp = client.post(
        "/dashboard",
        data=json.dumps({"title": "Diesel Jeans", "owner": user.id}),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"This field is required" in resp.data


def test_delete_product(test_app, test_database):
    recreate_db()
    user = add_user("elle", "elle@gmail.com", "password")
    client = test_app.test_client()
    resp = client.post(
        "/login",
        data=json.dumps({"username": "elle", "password": "password"}),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Hi, elle!" in resp.data

    resp = client.post(
        "/dashboard",
        data=json.dumps(
            {
                "title": "Diesel Jeans",
                "url": "https://poshmark.com/brand/Diesel",
                "owner": user.id,
            }
        ),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Diesel" in resp.data

    resp = client.post("/delete/1", follow_redirects=True)
    assert resp.status_code == 200
    assert b"Your search has been removed!" in resp.data
