import os
import pickle
from urllib.parse import urlparse
from datetime import date
from dotenv import load_dotenv
import psycopg2
from s3_helper import upload_file


result = urlparse(os.environ.get("DATABASE_URL"))
username = result.username
password = result.password
database = result.path[1:]
hostname = result.hostname

product_table = 'product'
user_table = 'users'
TODAY = date.today().strftime("%m-%d-%Y")


def get_db_conn():
    "Return database connection"
    try:
        conn = psycopg2.connect(database=database,
                                user=username,
                                password=password,
                                host=hostname)
        return conn
    except Exception:
        print("Unable to connect to the database")
        return None


def get_unique_users():
    "Return list of unique users"
    conn = get_db_conn()
    c = conn.cursor()
    c.execute("""SELECT id, username, email
                FROM {t};""".format(t=user_table))
    users = c.fetchall()
    conn.close()

    return users


def get_products(user):
    "Return list of products for a given user"
    conn = get_db_conn()
    c = conn.cursor()
    c.execute("""SELECT product.title, product.url
                FROM {t1}
                WHERE product.user_id = {t3};""".format(t1=product_table,
                                                        t2=user_table,
                                                        t3=user[0]))
    products = c.fetchall()
    conn.close()

    if len(products) == 0:
        return None

    return (user, products)


if __name__ == '__main__':
    users = get_unique_users()
    data_store = [get_products(user) for user in users]
    data_store = list(filter(None, data_store))
    filename = "etl-{date}.p".format(date=TODAY)
    pickle.dump(data_store, open(filename, "wb"))
    print('Uploaded: ', filename, 'to S3')
    upload_file(filename)
