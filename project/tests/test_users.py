import json

from project.tests.utils import add_user, recreate_db


def test_add_user(test_app, test_database):
    recreate_db()
    client = test_app.test_client()
    resp = client.post(
        "/register",
        data=json.dumps(
            {
                "username": "elle",
                "email": "elle@gmail.com",
                "password": "password",
                "password2": "password",
            }
        ),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Congratulations, you are now a registered user!" in resp.data


def test_add_user_missing_email(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/register",
        data=json.dumps(
            {
                "username": "elle",
                "email": "elle@gmail.com",
                "password": "password",
                "password2": "password55",
            }
        ),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Field must be equal to password." in resp.data


def test_login_user(test_app, test_database):
    recreate_db()
    add_user("elle", "elle@gmail.com", "password")
    client = test_app.test_client()
    resp = client.post(
        "/login",
        data=json.dumps({"username": "elle", "password": "password"}),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Hi, elle!" in resp.data


def test_login_user_wrong_password(test_app, test_database):
    recreate_db()
    add_user("elle", "elle@gmail.com", "password")
    client = test_app.test_client()
    resp = client.post(
        "/login",
        data=json.dumps({"username": "elle", "password": "hello"}),
        content_type="application/json",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"Invalid username or password" in resp.data
