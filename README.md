# Daily Posh App

[Poshmark](https://poshmark.com/) is a social commerce platform where users can buy and resell new or lightly used clothing items. The platform is great for someone who likes to stay on fashion trends without spending too much money.

One of my co-workers is an avid buyer and seller, however, she mentioned that Poshmark doesn’t have a saved search feature. Say you’re in the market for a certain brand and style of jeans, you would have to look up that exact search parameter every couple days or every week to check the recent listings.

I decided to build an application where users can save their unique searches and send them a daily email with new listings.  

[The app is deployed on Heroku](http://www.dailyposh.xyz/).

<br/>

## User Story

Here is an animated view of a user identifying a unique search on Poshmark, adding it to the app and then getting the email results.

![User Flow](figures/user-flow.gif)

<br/>

## Application Architecture

![App Flow](figures/app-flow.png)

- **Flask App**: The Flask app serves the frontend views to allow users to learn about the service, register/login as well as add product queries.
- **Database**: The database stores all product searches and user data.
- **ETL Worker**: This worker extracts data from the relational database, transforms it and exports it as a Python data structure.
- **Scraping Worker**: This worker uses the ETL export to scrape Poshmark pages and exports the results.
- **Mailing Worker**: This worker uses the scraping export to generate HTML markup and send emails to users.


<br/>

## Local Testing Instructions

Build the images:

> $ docker-compose build

Run the containers:

> $ docker-compose up -d

Create the database:

> $ docker-compose exec users python manage.py recreate_db
